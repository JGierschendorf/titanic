import keras
import os
import io
import math
import tensorflow as tf
import matplotlib.pyplot as plt
import seaborn as sns

from keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping, LearningRateScheduler, ReduceLROnPlateau
from sklearn.metrics import confusion_matrix

class TensorBoardCallback(keras.callbacks.Callback):

    def __init__(self, saving_path="", x_train=None, y_true=None, x_validation=None, y_validation=None):
        super(TensorBoardCallback, self).__init__()

        self.x_train = x_train
        self.y_true = y_true
        
        self.x_validation = x_validation
        self.y_validation = y_validation

        self._saving_path = saving_path
        self._logs_path = os.path.join(saving_path, 'logs')
        self._models_path = os.path.join(saving_path, 'model')
        self.__temp_model = self._models_path + '\MLP' + "_{epoch:02d}-{binary_accuracy:.8f}.h5"

        True if os.path.exists(self._models_path) else os.makedirs(self._models_path)

        self._file_writer = tf.summary.create_file_writer(self._logs_path)
        self._file_writer.set_as_default()

        self.tensorboard = TensorBoard(log_dir=self._logs_path,
                                       histogram_freq=1,
                                       write_grads=True,
                                       write_graph=True)

        self.checkpoint = ModelCheckpoint(self.__temp_model,
                                          monitor='binary_accuracy',
                                          verbose=0,
                                          save_best_only=False,
                                          save_weights_only=False,
                                          mode='min')

        self.early_stopping = EarlyStopping(monitor='val_binary_accuracy',
                                            patience=300,
                                            verbose=1,
                                            mode='auto')
        
        self.reduce_lr = ReduceLROnPlateau(monitor='val_loss', 
                                           factor=0.5, 
                                           patience=50, 
                                           min_lr=1e-9,
                                           verbose=1)

        self.add_learning_rate = True
        self.add_confusion_matrix = True

        print('Callback initialized')
        print('logs dir:', self._logs_path)
        print('models dir:', self._models_path)

    def __build_confusion_matrix(self):

        confusion_matrix_fig = plt.figure(num=1, figsize=(3, 3), dpi=160, facecolor='w')

        prediction = self.model.predict(self.x_train)
        prediction[prediction >= 0.5] = 1
        prediction[prediction < 0.5] = 0
        prediction = prediction.reshape(1, -1)[0]
        prediction = prediction.astype('int8')

        sns.heatmap(confusion_matrix(self.y_true, prediction),
                    linewidths=0.1,
                    linecolor='white',
                    annot=True,
                    fmt='d')

        plt.tight_layout()
        plt.ylabel('Predicted label')
        plt.xlabel('True label')

        return self.__plot_to_image(confusion_matrix_fig)

    def __plot_to_image(self, figure):
        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        plt.close(figure)
        buf.seek(0)
        image = tf.image.decode_png(buf.getvalue(), channels=4)
        image = tf.expand_dims(image, 0)
        return image

    def on_epoch_end(self, epoch, logs={}):

        if self.add_learning_rate:
            lr = float(tf.keras.backend.get_value(self.model.optimizer.learning_rate))
            tf.summary.scalar('learning rate', data=lr, step=epoch)

        if self.add_confusion_matrix:
            confusion_matrix = self.__build_confusion_matrix()
            tf.summary.image("Confusion Matrix", confusion_matrix, step=epoch)

        if epoch % 20 == 1:
            path = self._saving_path + '\MLP_' + str(epoch) + "-" + str(("{:.8f}").format(logs["binary_accuracy"])) + ".h5"
            self.model.save(path)

        self._file_writer.flush()

    def on_train_end(self, logs=None):
        print("on_train_end")
        